import { Injectable } from '@angular/core';
import { AbstractService } from './abstract.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService extends AbstractService {

  constructor(private http: HttpClient) {
    super();
  }

  public getAllUser() {
    return this.http.get(`http://localhost:8080/api/user/getAllUser`, { headers: this.getHeaders() })
  }
}
