package com.gamification.domain;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.gamification.enums.BadgeType;

import lombok.Data;

@Data
@Entity
public class Badge {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Integer id;
	private Integer userId;
	private Integer projectId;
	private LocalDateTime startTime;
	private LocalDateTime endTime;
	private BadgeType badgeType;
	

}
