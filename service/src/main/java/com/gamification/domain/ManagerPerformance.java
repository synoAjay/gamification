package com.gamification.domain;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class ManagerPerformance {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Integer id;
	private Integer userId;
	private Integer projectId;
	private Integer defectDensity;
	private Integer defectRemovalEfficiency;
	private Integer resourceUtilization;
	private LocalDateTime startTime;
	private LocalDateTime endTime;
	
	
}
