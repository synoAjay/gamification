package com.gamification.domain;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SecondaryTable;
import javax.persistence.Transient;
import javax.validation.constraints.Email;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@SecondaryTable(name="user_detail")
public class User implements UserDetails{

	private static final long serialVersionUID = 7635015366102497138L;

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Integer id; 
	@Email
	private String username;
	private String password;
	
	@Column(table="user_detail")
	private String firstName;
	
	@Column(table="user_detail")
	private String lastName;
	
	@Column(table="user_detail")
	private Integer departmentId;
	
	@Column(table="user_detail")
	private Integer teamId;
	
	@Column(table="user_detail")
	private Integer projectId;
	
	@OneToOne
	private Role role;
	
	@Transient
	private Collection<? extends GrantedAuthority> authorities;
	
	public static User build(User user) {
		List<GrantedAuthority> authorities = Arrays.asList(new SimpleGrantedAuthority(user.getRole().getName()));

		return new User(
				user.getId(), 
				user.getUsername(), 
				user.getPassword(), 
				authorities);
	}

	public User(Integer id, String username, String password, List<GrantedAuthority> authorities) {
		this.id=id;
		this.username=username;
		this.password=password;
		this.authorities=authorities;
	}

	public User(String username, String password) {
		this.username=username;
		this.password=password;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}
	
}
