package com.gamification.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gamification.domain.User;
import com.gamification.response.GamificationResponse;
import com.gamification.services.UserService;

@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	UserService userService;
	
	@GetMapping("/getAllUser")
	public GamificationResponse<List<User>>  getAllUser(){
		return new GamificationResponse<List<User>>(userService.getFindAll());
	}
}
