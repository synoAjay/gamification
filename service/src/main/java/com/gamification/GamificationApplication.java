package com.gamification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.gamification.domain.User;
import com.gamification.repository.UserRepository;
import com.gamification.repository.mongo.MongoUserRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
@EnableMongoRepositories(basePackages = "com.gamification.repository.mongo")
public class GamificationApplication implements CommandLineRunner{

	@Autowired
	MongoUserRepository mongoUserRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(GamificationApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		com.gamification.domain.mongo.User mongoUser =  new com.gamification.domain.mongo.User();
		mongoUser.setUserName("hellBoy");
		mongoUserRepository.save(mongoUser);
		log.info("2.user->"+mongoUser.getId());
		
	}

}
