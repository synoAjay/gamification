package com.gamification.repository.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.gamification.domain.mongo.User;

@Repository
public interface MongoUserRepository extends MongoRepository<User, String>{

}
