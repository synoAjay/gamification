package com.gamification.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

@Component
public class AdminCORSFilter implements Filter{

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		String method = httpServletRequest.getMethod();
		
			HttpServletResponse httpResponse = (HttpServletResponse) response;
			httpResponse.setHeader("Access-Control-Allow-Origin", "*");
			httpResponse.setHeader("Access-Control-Allow-Methods", "*");
			httpResponse.setHeader("Access-Control-Allow-Headers", "*");
			httpResponse.setHeader("Access-Control-Expose-Headers","Authorization");
			if ("OPTIONS".equalsIgnoreCase(method)) {
				httpResponse.setHeader("Access-Control-Allow-Credentials", "false");
			}
			chain.doFilter(request, response);
		
	}

}
