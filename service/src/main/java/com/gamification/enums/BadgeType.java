package com.gamification.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum BadgeType {
	GOLD("GOLD");
	
	@Getter private String value;
}
