package com.gamification.response;

import lombok.Data;

@Data
public class GamificationResponse<T> {
	
	private T data;
	private String status = "SUCCESS";
	
	public GamificationResponse(T data) {
		this.data=data;
	}
	
}
